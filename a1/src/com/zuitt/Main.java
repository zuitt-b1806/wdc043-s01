package com.zuitt;
import java.util.Scanner;
import java.math.BigDecimal;


public class Main {
    public static void main(String[] args) {

        Scanner scannerName = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = scannerName.nextLine();

        System.out.println("Last Name");
        String lastName = scannerName.nextLine();

        String fullName = firstName + lastName;

        System.out.println("Enter your Grades to get an Average");

        System.out.println("First Subj. Grade:");
        double firstGrade = scannerName.nextDouble();

        System.out.println("2nd Subj. Grade");
        double secondGrade = scannerName.nextDouble();

        System.out.println("3rd Subj. Grade");
        double thirdGrade = scannerName.nextDouble();

        double avgGrade = (firstGrade + secondGrade + thirdGrade)/3;

        BigDecimal bd = new BigDecimal(avgGrade);
        double avgGrade2 = bd.doubleValue();

        System.out.println("Good Day, "+ fullName);
        System.out.println("Your grade Average is "+ avgGrade2);

    }
}
