package com.zuitt.example;
import java.util.Scanner;
public class Main {
   /*
    Main Class - is the entry point of our Java Program and is responsible for executing our code.

        Every Java program should have atleast 1 class and 1 function inside it.

   */

    public static void main(String[] args) {
        /*
        *   Main Function is where most executable code
        *
        *   you can actually create an intelliJ Java project with templates that already uses or has a main class and function.
        *
        *   Public keyword is what we call an access modifier
        *
        *   The void is the return statement’s data type of the main    function that it designs what kind of data will be      returned.
            Void means that the main function will return nothing.
            *
            *
            *
            *
        *
        *   In Java, to be able to declare a variable, we have to identify or declare its data type. which means that, the data types on the variable will expect and only accept data with the type declared.
        * */

        int myNum;
        myNum = 3;

        System.out.println(myNum);

//        myNum = "sample string";

        myNum = 50;
        System.out.println(myNum);


        System.out.println("Hello World!");
        //byte = -128 to 127, anything beyond this range will not be recognized by byte but will automatically recognize as int
        byte students = 127;
        //short = -43,768 to 32,767
        short seats = 32767;

        System.out.println(students);
        System.out.println(seats);

        int localPopulation = 2147483647;

        long worldPopulation = 7862881145L;

        System.out.println(localPopulation);
        System.out.println(worldPopulation);

        float piFloat = 3.1415923f;
    //  float can only have up to 7 decimal places and final decimal place is being rounded off.
        double piDouble = 3.1415923;

        System.out.println(piFloat);
        System.out.println(piDouble);

        char letter = 'a';
        letter = 'b';

        System.out.println(letter);

        //boolean
        boolean isMVP = true;
        boolean isChampion = false;

        System.out.println(isMVP);
        System.out.println(isChampion);

        //final = creates a constant variable - same as in JS, cannot be re-assigned;

        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //String - in Java, is non-primitive Data. Because in Java, Strings are objects that can use methods.
        //Non-primitive data types have access to methods.
        //ex: Array, Class, Strings, Interface

        String username = "chesterSmith29";
        System.out.println(username);
        System.out.println(username.isEmpty());
        //.isEmpty() is a method of a string which returns a boolean.
        //It will check the length of the string and return true, if the length is 0 or will return false otherwise.

        username = "";
        System.out.println(username.isEmpty());

        //scanner is a class which allows us to input into our progra, it is similar to prompt() in JS.
        //However, scanner, being class, has to be imported to be used.

        Scanner scannerName = new Scanner(System.in);
        System.out.println("What is your Name?:");

        String myName = scannerName.nextLine();

        System.out.println("Your name is " + myName + "!");

        System.out.println("What is your age?:");
        int myAge = scannerName.nextInt();

        System.out.println("You are " + myAge + "Years Old!");

        System.out.println("What is your first quarter grade?:");
        double firstQuarter = scannerName.nextDouble();

        System.out.println("Your grade is " + firstQuarter +"!");

        System.out.println("Input a number:");

        int num1 = scannerName.nextInt();
        System.out.println("Input a 2nd NUmber:");

        int num2 = scannerName.nextInt();
        //much lik ein javascript we can use operators like + / * - or mathematical operations
        int sum = num1 + num2;


        System.out.println("The sum of the 2 inputted numbers are: "+ sum);

        /* Mini Activity:
        *   1.)Add a system out print line to ask the user for a number. Create a new integer variable and store the value returned by using the scanner  and its nextInt() method.
        *
        * 2.) Add a system out print line to ask the user for a number. Create a new integer variable and store the value returned by using the scanner and its nextInt() method.
        *
        * 3.) Create a new integer variable to store the difference of the numbers that was input.
        *
        * 4.) Print the value of the difference between the input numbers.
        *
        * */
        System.out.println("Subtract a number:");
        System.out.println("Input a number:");

        int num3 = scannerName.nextInt();
        System.out.println("Input a 2nd NUmber:");

        int num4 = scannerName.nextInt();
        //much lik ein javascript we can use operators like + / * - or mathematical operations
        int diff = num3 - num4;


        System.out.println("The difference of the 2 inputted numbers are: "+ diff);

    }

}
